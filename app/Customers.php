<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Customers extends Model
{
    //
	protected $connection = 'tenant';
	
	protected $table = '';
	
    protected $fillable = ['id', 'attribute_name', 'short_code', 'customer_id', 'created_by', 'modified_by', 'active_flag', 'users.name'];
	
	//Function to get values
	//Input : NA
	//Output : NA
	public function get_customer($params){

        $cin = $params['cin'];

        $customer = DB::table('customers as cus')
        ->join('login_type as lt', 'cus.login_type_flag', 'lt.id')
        ->select('cus.id', 'cus.cin', 'lt.type_name', 'cus.login_type_flag', 'lt.login_type')
        ->where('cus.cin', $cin)->where('cus.active_flag', 1)->first();
           
        $res['status'] = 0;
        if($customer){
            $res['status'] = 1;
            $res['cin'] = $customer->cin;
            $res['customer_id'] = $customer->id;
            $res['type_name'] = $customer->type_name;
            $res['login_type_flag'] = $customer->login_type_flag;
            $res['login_type'] = $customer->login_type;
            $res['login_type_name'] = "Enter register ".$customer->type_name;
        }
		
		return $res;
	}
}
