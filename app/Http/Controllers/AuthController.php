<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use DB;

class AuthController extends Controller
{
    /** Use :  Construct function **/
    public function __construct() {
    	
        $this->user = new Users();
		
    }

    /**
     * get a application config
     *
     * @return json
     */
    public function user_login(Request $request){

        $requestData = $request->all();

        

        if(isset($requestData['login_type']) && isset($requestData['type_value']) && isset($requestData['login_type_flag'])){

            $results['status'] = 0;
            $results['message'] = "User get failed";
            $get_res = $this->user->user_login($requestData);

            if($get_res['status'] == 1){
                $results['status'] = 1;
                $results['message'] = "User get success";
                $results['customer'] = $get_res;
            }
            

        }else{

            $results['status'] = 4;
            $results['message'] = "Required field is missing";
            
        }
        
        echo json_encode($results);
    }

    public function api_decrypt($params_url){

        print_r(base64_decode($params_url));
        $url = base64_decode($params_url);
        return redirect($url);

    }

   
}