<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeHome;
use DB;

class EmployeeHomeController extends Controller
{
    /** Use :  Construct function **/
    public function __construct() {
    	
        $this->employee_home = new EmployeeHome();
		
    }

    /**
     * get a application config
     *
     * @return json
     */
    public function employee_home(Request $request){

        $requestData = $request->all();
        
        if(isset($requestData['employee_id'])){

            $employee_id = $requestData['employee_id'];

            $results['status'] = 0;
            $results['message'] = "Employee get failed";
            $get_res = $this->employee_home->employee_home($employee_id);

            if($get_res['status'] == 1){
                $results['status'] = 1;
                $results['message'] = "Emplyee get success";
                $results['customer'] = $get_res;
            }
            
            

        }else{

            $results['status'] = 4;
            $results['message'] = "Required field is missing";
            
        }
        
        echo json_encode($results);
    }

    public function api_decrypt($params_url){

        print_r(base64_decode($params_url));
        $url = base64_decode($params_url);
        return redirect($url);

    }

   
}