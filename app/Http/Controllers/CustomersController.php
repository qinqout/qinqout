<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;

class CustomersController extends Controller
{
    /** Use :  Construct function **/
    public function __construct() {
    	
        $this->customer = new Customers();
		
    }

    /**
     * get a application config
     *
     * @return json
     */
    public function identify_customer(Request $request){

        $requestData = $request->all();

        if(isset($requestData['cin']) && $requestData['cin'] != ""){
            $results['status'] = 0;
            $results['message'] = "Customer get failed";
            $get_res = $this->customer->get_customer($requestData);

            if($get_res['status'] == 1){
                $results['status'] = 1;
                $results['message'] = "Customer get success";
                $results['customer'] = $get_res;
            }

        }else{
            $results['status'] = 4;
            $results['message'] = "Please enter CIN";
        }

        
        echo json_encode($results);
    }

    public function api_decrypt($params_url){

        print_r(base64_decode($params_url));
        $url = base64_decode($params_url);
        return redirect($url);

    }

    public function user_login(){
        echo 1;exit;
    }
}