<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApplicationConfig;

class ApplicationConfigController extends Controller
{
    /** Use :  Construct function **/
    public function __construct() {
    	
        $this->app_config = new ApplicationConfig();
		
    }

    /**
     * get a application config
     *
     * @return json
     */
    public function application_config(){
        $result = $this->app_config->get_configs();
        echo json_encode($result);
    }

    public function api_decrypt($params_url){

        print_r(base64_decode($params_url));
        $url = base64_decode($params_url);
        return redirect($url);

    }

    public function user_login(){
        echo 1;exit;
    }
}