<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Users extends Authenticatable
{
    use HasApiTokens, Notifiable;
    //
	protected $connection = 'tenant';
	
	protected $table = '';
	
    protected $fillable = ['id', 'attribute_name', 'short_code', 'customer_id', 'created_by', 'modified_by', 'active_flag', 'users.name'];
	
	//Function to get values
	//Input : NA
	//Output : NA
	public function user_login($params){

        $login_type = $params['login_type'];
        $type_value = $params['type_value'];
        $login_type_flag = $params['login_type_flag'];
        $customer_id = $params['customer_id'];


        $users = DB::table('users')->where($login_type, $type_value)->where('customer_id', $customer_id)->first();

        $res['status'] = 0;
        $res['message'] = "User get failed";

        if($users){
            $res['status'] = 1;
            $res['message'] = "User get sucess";
            $res['user'] = $users;
        }

        $res['OTP'] = '201901';

		return $res;
	}
}
