<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApplicationConfig extends Model
{
    //
	protected $connection = 'tenant';
	
	protected $table = '';
	
    protected $fillable = ['id', 'attribute_name', 'short_code', 'customer_id', 'created_by', 'modified_by', 'active_flag', 'users.name'];
	
	//Function to get values
	//Input : NA
	//Output : NA
	public function get_configs(){

		$app_config = DB::table('application_config')->first();
		
		return $app_config;
	}
}
