<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeeHome extends Model
{
    //
	protected $connection = 'tenant';
	
	protected $table = '';
	
    protected $fillable = ['id', 'attribute_name', 'short_code', 'customer_id', 'created_by', 'modified_by', 'active_flag', 'users.name'];
	
	//Function to get values
	//Input : NA
	//Output : NA
	public function employee_home($employee_id){

        
        $employees = DB::table('employees as emp')->where('emp.id', $employee_id)->where('emp.active_flag', 1)
        ->join('shifts as s', 'emp.shift_timing', 's.id')
        ->select('emp.id as employee_id', 'emp.emp_id', 'emp.email', 'emp.mobile', 'emp.profile_image', 's.from_time as shift_from_time', 's.to_time as shift_to_time')
        ->first();

        $res['status'] = 0;
        $res['message'] = "Employee get failed";

        if($employees){
            $res['status'] = 1;
            $res['message'] = "Employee get sucess";
            $res['employee'] = $employees;
        }

        print_r($res);exit;

		return $res;
	}
}
