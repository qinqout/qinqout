<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/application_config', 'ApplicationConfigController@application_config');

Route::any('/identify_customer', 'CustomersController@identify_customer');

Route::any('/user_login', 'AuthController@user_login');
Route::any('/resend_otp', 'AuthController@resend_otp');
Route::any('/pin_set', 'AuthController@pin_set');

Route::any('/employee_home', 'EmployeeHomeController@employee_home');

//Route::any('/v1/{params_url}', 'ApplicationConfigController@api_decrypt');

//Route::any('/user_login', 'ApplicationConfigController@user_login');
